/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.mcc.lab4;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class Ch14JButtonJFrame extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JButton button, button2;
    private JTextField pesan;

    public static void main(String[] args) {
        Ch14JButtonJFrame frame = new Ch14JButtonJFrame();
        frame.setVisible(true);
    }

    public Ch14JButtonJFrame() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        // set the frame properties
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Program Ch7JButtonFrame");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        // create and place two buttons on the frame's content pane
        button = new JButton("Click Me");
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);

        button2 = new JButton("Tombol2");
        button2.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button2);

        // register 'Exit upon closing' as a default close operation
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // registering a ButtonHandler as an action listener of the two buttons
        ButtonHandler handler = new ButtonHandler();
        button.addActionListener(this);
        button2.addActionListener(this);

        pesan = new JTextField();
        pesan.setColumns(20);
        contentPane.add(pesan);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedButton = (JButton) e.getSource();

        JRootPane rootPane = clickedButton.getRootPane();
        Frame frame = (JFrame) rootPane.getParent();
        String buttonText = clickedButton.getText();

        if (buttonText.equals("Click Me")) {
            String isiPesan = pesan.getText();
            frame.setTitle(isiPesan);
        } else {
            frame.setTitle("You clicked " + buttonText);
        }
    }
}
